import React, { Component } from 'react';
import { Link } from 'react-router';

class NavBar extends Component {
  render() {
    return (
        <nav className="navbar navbar-default navbar-fixed-top">
          <div className="navbar-header">
             <Link className="navbar-brand" to="#">
               <img  alt="Data Greet" src="img/datagreet-tiny.png" style={{width: '35px', height: '35px'}}/>
             </Link>
             <ul className="nav navbar-nav">
                 <li><a className="navbar-link" href="#">Data Greet</a></li>
              </ul>
           </div>
           <div className="collapse navbar-collapse">
               <ul className="nav navbar-nav navbar pull-right" role="group">
                   <li><Link to="/apiInfo">Api Info</Link></li>
                   <li><Link to="/newApi">Api Add</Link></li>
                   <li><a href="#">Setting</a></li>
                   <li><div className="btn-group" >
                           <a href="#" className="btn btn-success navbar-btn">Logon</a>
                           <a href="#" className="btn btn-primary navbar-btn">Signup</a>
                       </div>
                   </li>
               </ul>
           </div>
        </nav>
    );
  }
}

export default NavBar;
