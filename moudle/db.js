var mysql = require('mysql');
var db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'jevy',
  database: 'foodie'
});

db.connect(function(err) {
  if (err) {
    console.log("DB connect error ...");
    throw err;
  }
});

db.shopInfo = function(idshop, callback) {
  // console.log('idshop: ', idshop);
  db.query('SELECT * from shop where idshop = ?', [idshop], function(
    err, rows) {
      // console.log('Err: ', err);
    // console.log('The solution is: ', rows);
    callback(err, rows);
  });
};

db.addShopInfo = function(data, callback) {
  db.query(
    'Insert into set ? ', [data],
    function(err, result) {
      callback(err, result);
    });
};
db.editShopInfo = function(data,callback){
  db.query(
    'update shop set name=?, phone=?, email=?, initday=?, starttime=?, endtime=? where idshop=?' ,
    [data.name, data.phone, data.email, data.initday, data.starttime,data.endtime,data.idshop],
    function(err, result) {
      console.log("Edit shop info: "+err);
      callback(err, result);
    });
};

db.getStaff = function(data, callback){
  db.query('select *  from staff where idshop = ?',
  [data],  function(err, result) {
    console.log(err);
    console.log(result);
    callback(err, result);
  });
};

db.addStaff = function(data, callback){
  // console.log(data);
  db.query('insert into staff set ?',
  [data],  function(err, result) {
    console.log("Insert restult of staff: "+ err + result);
    callback(err, result);
  });
};
db.editStaff = function(data, callback){
  var sql = "update staff set name=?, email=?, basesalary=?, phone=? where idstaff=?";
  db.query(sql, [data.name, data.email, data.basesalary, data.phone, data.idstaff], function(err, result) {
    callback(err, result);
  });
};
db.deleteStaff = function(data, callback){
  var sql = "delete from staff where idstaff = ?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};

db.getRandom = function(data, callback){
  var sql = "select * from random where idrandom=?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};

db.addRandom = function(data, callback){
  var sql = "insert into random set ?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};
db.randomCheck = function(data, callback){
  var sql = "select idrandom from random where idrandom=? and random = ?";
  db.query(sql, [data.idrandom, data.random], function(err, result){
    if(result.length <1){
      callback("error", result);
    }else{
      callback(err, result);
    }

  });
};
db.getMailSendFlag = function(data, callback){
  var sql = "select * from random where idrandom = ? and idstaff=?";
  console.log("DB params getMailSendFlag");
  console.log(data);
  db.query(sql, [data.idrandom, data.idstaff], function(err, result){
    callback(err, result);
  });
};
db.setMailSendFlag = function(data, callback){
  var sql = "update random set sendflag='Y' where idrandom=?";
  db.query(sql, [data.idrandom], function(err, result) {
    callback(err, result);
  });
};
db.updateFeedbackFlag = function(data, callback){
  //Check whereter updated
  var query = "select feedbackflag from random where idrandom=?";
  db.query(query, [data.idrandom], function(err, result){
    console.log("Query feedbackflag:"+result.length);
    if(result!="N"){
      //Set feedback flag if the flag is N
      var sql = "update random set feedbackflag='Y' where idrandom=?";
      db.query(sql, [data.idrandom], function(err, result) {
        callback(err, result);
      });
    }else{
      //Linkage has been used
      callback("error", "The Linkage has been used. Please Contact your manager if you have any issue.");
    }
  });

};

db.addShiftHandler = function(data, callback){
  var sql = "insert into shifthandler set ?";
  console.log("addShiftHandler data:");
  console.log(data);
  db.query(sql, [data], function(err, result) {
    console.log(err+result);
    callback(err, result);
  });
};

db.getShiftHandler = function(data, callback){
  var sql = "select * from shifthandler where idshifthandler=?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};

db.addSalaryRate = function(data, callback){
  var sql = "insert into salaryrate set ?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};

db.editSalaryRate = function(data, callback){
  var sql = "update salaryrate set ?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};

db.removeShiftSet = function(data, callback){
  var sql = "delete from shiftset where idshop=? and shiftDay=? and seq=?";
  console.log(data);
  db.query(sql, [data.idshop, data.shiftDay, data.seq], function(err, result){
    console.log(err);
    console.log(result);
    callback(err, result);
  });
};

db.addShiftSet = function(data, callback){
  var querySQL = "select * from shiftset where idshop=?  and shiftDay=? and seq=?";
  db.query(querySQL, [data.idshop, data.shiftDay, data.seq], function(err, result){
    console.log("select: "+err+":"+result);
    console.log(typeof result);
    console.log(data);
    if(result.length===0){
      var sql = "insert into shiftset set ?";
      db.query(sql, [data], function(err, result) {
        // console.log("insert: "+err+":"+result);
        callback(err, result);
      });
    }else{
      //Select record exist
      var setSQL = "update shiftset set start=?, end=? where idshop=? and shiftDay=? and seq=?";
      var d = [data.start, data.end, data.idshop, data.shiftDay, data.seq];
      console.log(d);
      db.query(setSQL, d, function(err, result){
        // console.log("update: "+err+":"+result);
        callback(err, result);
      });

    }
  });

};

db.getShiftSet = function(data, callback){
  var sql = "select * from shiftset where idshop=?";
  // console.log(sql+":"+data);
  db.query(sql, [data], function(err, result) {
    // console.log("get shiftset: "+err+":"+result);
    callback(err, result);
  });
};

db.getSingleShiftSet = function(data, callback){
  var sql = "select * from shiftset where idshop=? and shiftDay=? and seq=?";
  // console.log(sql+":"+data);
  db.query(sql, data, function(err, result) {
    // console.log("get shiftset: "+err+":"+result);
    callback(err, result);
  });
};

db.addShifts = function(data, callback){
  var sql = "insert into shifts set ?";
  console.log(data);
  db.query(sql, [data], function(err, result) {
    callback(err, result);
  });
};

db.editShifts = function(data, callback){
  var sql = "update shifts set ?";
  db.query(sql, [data], function(err, result) {
    callback(err, result);

  });
};

db.getCurrentShifts = function(data, callback) {
  var sql = "select * from shifts where idshifthandler = ? order by shiftDay, seq";
  db.query(sql, [data], function(err, result) {
    if(!err){
      callback(err, result);
    }

  });
};

db.getCurrentHandler = function(data, callback) {
  console.log(data);
  var sql = "select MAX(idshifthandler) as currentShiftHandler from shifthandler where idshop = ?";
  db.query(sql, [data], function(err, result) {
    console.log(err);
    callback(err, result);
  });
};



module.exports = db;
