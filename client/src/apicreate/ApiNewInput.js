import React from 'react';

const ApiNewInput = (props) => {
  return (
    <div>
      <form onSubmit={props.onSubmit}>
        <h1 className=""> New API Service </h1>
        <div className="form-group">
          <label className="control-label"> API Service Name </label>
          <input
            onChange={props.onChange}
            type="text"
            name="serviceName"
            className="form-control"
          />
        </div>

        <div className="form-group">
          <button className="submit-button btn btn-default"> Create
          </button>
        </div>
      </form>
    </div>
  );
};

ApiNewInput.propTypes = {
  error: React.PropTypes.object,
  onChange: React.PropTypes.func,
  onSubmit: React.PropTypes.func,
};

export default ApiNewInput;
