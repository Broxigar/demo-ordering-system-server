import React, { Component } from 'react';
import { Link } from 'react-router';
import './App.css';
import ApiInfo from './apidisplay/ApiInfo';
import NavBar from './NavBar';


const App = (props) =>  {
    return (
      <div className="container">
        {props.children}
      </div>
    );
}


App.propTypes = {
  children: React.PropTypes.node,
};

export default App;
