FROM node:8

RUN mkdir -p /server
WORKDIR /server
COPY . .

RUN npm install

#RUN cd client && npm install && npm run build  && cd ..


CMD ["npm", "start"]

