var dbServices = require('../moudle/db-service');
var bcrypt = require('bcrypt-nodejs');
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
var jwt = require('jsonwebtoken');
var passportJWT = require("passport-jwt");
var dateFormat = require('dateformat');
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;
var moment = require('moment');

    passport.use('user', new LocalStrategy(
        {
            usernameField: 'name',
            passwordField: 'password'
          },
        function(username, password, done) {

        //console.log(username)
            var sql = 'select * from operators where name = ?';
            //console.log("User input: ",username, password)
            dbServices.query(sql, [username], function(error, user){  

            //console.log(dd)
            if(error){
                //console.log("abc")
                return done(null, false, { message: 'Incorrect username.' });
        
            }else if (user.length === 0){
                //console.log("cab")
                return done(null, false, { message: 'Incorrect username.' });
            }else{
                //console.log("aaaa")
                bcrypt.compare(password, user[0].password, function (err, isMatch) {  
                    if (err) {
                        //console.log("bbbb")
                        return done(null, false, { message: 'Incorrect password.' });
                    }else if(!isMatch){
                        //console.log("ccccc")
                        return done(null, false, { message: 'Incorrect password.' });
                    }else{
                        
                        //var day = dateFormat(new Date(),"yyyy-mm-dd'T'HH:MM:ss");
                        var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss");
                        //console.log(user[0].id+" "+day)
                        var sqll = 'INSERT INTO loginLog (userID, time, userType) VALUES (?, ?, ?)';
                            dbServices.query(sqll, [user[0].id, day, 1], function(error){  
                                if(error){
                                    console.log("eee")
                                    return done(null, false, { message: error });
                            
                                }
                                else {
                                     //console.log("12")
                                    var payload = {id: user[0].id};
                                    
                                    var token = jwt.sign(payload, 'NovaMingUser', { expiresIn: 604800 });
                                    var user_f = ({
                                        "id": user[0].id,
                                        "name": user[0].name,
                                        "restaurantID": user[0].restaurantID,
                                        "Token": token 
                                    })
                                    

                                    
                                    //console.log("123")
                                    return done(null, user_f);
                                }
                    
                            });
                       
                    }
                });
            
            }
        });
  
        }
      ));
    //   if (password === user[0].password){
                
    //     var payload = {id: user[0].id};
                
    //             var token = jwt.sign(payload, 'NovaMingUser', { expiresIn: 604800 });
    //             //129600
    //             var user_f = ({
    //                 "id": user[0].id,
    //                 "name": user[0].name,
    //                 "restaurantID": user[0].restaurantID,
    //                 "Token": token 
    //             })
                
    //             //console.log(user_f)
    //             return done(null, user_f);
    // }
    // else {
    //     return done(null, false, { message: 'Incorrect password.' });
    // } 


      passport.use('admin', new LocalStrategy(
        {
            usernameField: 'name',
            passwordField: 'password'
          },
        function(username, password, done) {

            
            var sql = 'select * from admins where name = ?';
            //console.log("User input: ",username, password)
            dbServices.query(sql, [username], function(error, user){  

            //console.log(dd)
            if(error){
                return done(null, false, { message: 'Incorrect username.' });
        
            }else if (user.length === 0){
                return done(null, false, { message: 'Incorrect username.' });
            }else{
                //console.log("aaaa")
                bcrypt.compare(password, user[0].password, function (err, isMatch) {  
                    if (err) {
                        //console.log("bbbb")
                        return done(null, false, { message: 'Incorrect password.' });
                    }
                    else if(!isMatch){
                        //console.log("ccccc")
                        return done(null, false, { message: 'Incorrect password.' });
                    }
                    else{
                        
                        //var day = dateFormat(new Date(),"yyyy-mm-dd'T'HH:MM:ss");
                        var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss");
                        //console.log(user[0].id+" "+day)
                        var sqll = 'INSERT INTO loginLog (userID, time, userType) VALUES (?, ?, ?)';
                            dbServices.query(sqll, [user[0].id, day, 2], function(error){  
                                if(error){
                                    //console.log("eee")
                                    return done(null, false, { message: error });
                            
                                }
                                else {
                                     //console.log("12")
                                    var payload = {id: user[0].id};
                                    
                                    var token = jwt.sign(payload, 'NovaMingAdmin', { expiresIn: 604800 });
                                    var user_f = ({
                                        "id": user[0].id,
                                        "name": user[0].name,
                                        "restaurantID": user[0].restaurantID,
                                        "Token": token 
                                    })    
                                    //console.log("123")
                                    return done(null, user_f);
                                }
                    
                            });
                // if (password === user[0].password){
                   
                //     var payload = {id: user[0].id};
                            
                //             var token = jwt.sign(payload, 'NovaMingAdmin', { expiresIn: 129600 });
                //             var user_f = ({
                //                 "id": user[0].id,
                //                 "name": user[0].name,
                //                 "restaurantID": user[0].restaurantID,
                //                 "Token": token 
                //             })
                            
                //             //console.log(user_f)
                //             return done(null, user_f);
                // }
                // else {
                //     return done(null, false, { message: 'Incorrect password.' });
                // } 

            //console.log(dd[0].PASSWORD)
            //console.log(JSON.parse(JSON.stringify(user[0])))
                // bcrypt.compare(password, user[0].password, function (err, isMatch) {  
                //     if (err) {
                //         return done(null, false, { message: 'Incorrect password.' });
                //     }else if(!isMatch){
                //         return done(null, false, { message: 'Incorrect password.' });
                //     }else{
                        
                //         var payload = {id: user[0].id};
                        
                //         var token = jwt.sign(payload, 'NovaMing', { expiresIn: 129600 });
                //         var user_f = ({
                //             "id": user[0].id,
                //             "name": user[0].name,
                //             "restaurantID": user[0].restaurantID,
                //             "Token": token 
                //         })
                        
                        
                //         return done(null, user_f);
                //     }
                // });

            }
        })
  
        }
        })
        }
      ));

      passport.serializeUser(function(user, done) {
          
            done(null, user);
          }
      );
    
      passport.deserializeUser(function(user, done) {
        // if (isUser(user)){
        //     var sql = 'select * from operators where id = ?';
        //     dbServices.query(sql, user.id, function(error, dd){
        //     done(error, dd[0]);
        //     });
        // }
        //   else if (isAdmin(user)){
        //     var sql = 'select * from admins where id = ?';
        //     dbServices.query(sql, user.id, function(error, dd){
        //     done(error, dd[0]);
        //     });
        // }
        done(null, user);
      });

      //Google authentication
      passport.use(new GoogleStrategy({
        clientID: '41523621823-o5l6k4msc7pfj26aip4ut4ira2cdbilh.apps.googleusercontent.com',
        clientSecret: '3BQ5hs1Yw_noVnGeWj-gkp9d',
        callbackURL: '/users/auth/google/callback'
      },
      function(accessToken, refreshToken, profile, done) {  
        var sql = 'select * from USER where email = ?'
        var sqlIn = 'insert into USER set ?';
        //console.log("User input: ",username, password)
        //console.log('AAA '+profile.emails[0].value)
        //console.log('BBB '+profile.id)
        //console.log(profile.photos[0].value)
        dbServices.query(sql, [profile.emails[0].value], function(error, user){
            console.log(user)
            if (error) {
                console.log(error)
                return done(null, false, {message: error})
            }
            else if (user.length >0){
                var payload = {id: user[0].idUSER};
                        
                        var token = jwt.sign(payload, 'NovaMing', { expiresIn: 129600 });
                        var user_g = ({
                            "idUSER": user[0].idUSER,
                            "name": user[0].name,
                            "email": user[0].email,
                            "phone": user[0].phone,
                            "postLimit": user[0].postLimit,
                            "type": user[0].type,
                            "code": user[0].code,
                            "codeExpire": user[0].codeExpire,
                            "image": user[0].image,
                            "address": user[0].address,
                            "idSHOP": user[0].idSHOP,
                            "HIDDENEMAIL": user[0].HIDDENEMAIL,
                            "HIDDENPHONE": user[0].HIDDENPHONE,
                            "Token": token 
                        })
                    return done(null, user_g);
            }
            else {
                const userInfor = ({"email": profile.emails[0].value, 
                                    "name": profile.id, "password":'google',
                                    "image": profile.photos.values
                                });
                //console.log('DDD' +profile.photos.values)
                dbServices.query(sqlIn, [userInfor], function(error){
                    console.log(error)
                    if(error){
                      return done(null, false, {message: 'Insert error'});
                    }
                    else{
                        dbServices.query(sql, [profile.emails[0].value], function(error, user){
                            console.log(error)
                            if(error){
                              return error;
                            }
                            else{
                        
                                var payload = {id: user[0].idUSER};
                        
                                var token = jwt.sign(payload, 'NovaMing', { expiresIn: 129600 });
                                var user_g = ({
                                    "idUSER": user[0].idUSER,
                                    "name": user[0].name,
                                    "email": user[0].email,
                                    "phone": user[0].phone,
                                    "postLimit": user[0].postLimit,
                                    "type": user[0].type,
                                    "code": user[0].code,
                                    "codeExpire": user[0].codeExpire,
                                    "image": user[0].image,
                                    "address": user[0].address,
                                    "idSHOP": user[0].idSHOP,
                                    "HIDDENEMAIL": user[0].HIDDENEMAIL,
                                    "HIDDENPHONE": user[0].HIDDENPHONE,
                                    "Token": token 
                                })
                                return done(null, user_g);
                            }
                            })
                    }
                  });
            }
        }
        )
        //    User.findOrCreate({ googleId: profile.id }, function (err, user) {
        //      return done(err, user);
        //    });
      }
    ));

    //Facebook authentication
    passport.use(new FacebookStrategy({
        clientID: 282769358926817,
        clientSecret: "1ec59b682f6fa86f2829768efa01af56",
        callbackURL: "http://www.example.com/auth/facebook/callback"
      },
      function(accessToken, refreshToken, profile, done) {
        User.findOrCreate({facebookId: profile.id}, function(err, user) {
          if (err) { return done(err); }
          done(null, user);
        });
      }
    ));


    //Authentication
    var jwtOptionsUser = {}
    jwtOptionsUser.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    jwtOptionsUser.secretOrKey = "NovaMingUser";
    passport.use('jwtUser', new JwtStrategy(jwtOptionsUser, function(jwt_payload, next) {
    //console.log('payload received', jwt_payload);
    // usually this would be a database call:
    var sql = 'select * from operators where id = ?'
    dbServices.query(sql, [jwt_payload.id], function(error, user){
        //console.log(user)
        if (error) {
            console.log(error);
            console.log("aaaaaaaaaa")
            next(null, false);
        }
        else if (user.length>0){
            console.log("bbbbbbba")
               next(null, user[0]);
        }
        else {
            console.log("Ffffffffffff")
            next(new Error("User not found"),false,{ message: 'User not found.' });
        }
    })
    }
));

var jwtOptionsAdmin = {}
    jwtOptionsAdmin.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    jwtOptionsAdmin.secretOrKey = "NovaMingAdmin";
passport.use('jwtAdmin', new JwtStrategy(jwtOptionsAdmin, function(jwt_payload, next) {
    //console.log('payload received', jwt_payload);
    // usually this would be a database call:
    var sql = 'select * from admins where id = ?'
    dbServices.query(sql, [jwt_payload.id], function(error, user){
        //console.log(user)
        if (error) {
            console.log(error);
            next(null, false);
        }
        else if (user.length>0){
            
               next(null, user[0]);
        }
        else {
            next(new Error("User not found"),false,{ message: 'User not found.' });
        }
    })
    }
));

    


    module.exports = passport