import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import {Provider} from "react-redux";


import routes from './routes';
import App from './App';
import './index.css';


const mathReducer = (state = {
    result: 1,
    lastValues: []
}, action) => {
    switch (action.type) {
        case "ADD":
            state = {
                ...state,
                result: state.result + action.payload,
                lastValues: [...state.lastValues, action.payload]
            };
            break;
        case "SUBTRACT":
            state = {
                ...state,
                result: state.result - action.payload,
                lastValues: [...state.lastValues, action.payload]
            };
            break;
    }
    return state;
};

const userReducer = (state = {
    name: "Max",
    age: 27
}, action) => {
    switch (action.type) {
        case "SET_NAME":
            state = {
                ...state,
                name: action.payload
            };
            break;
        case "SET_AGE":
            state = {
                ...state,
                age: action.payload
            };
            break;
    }
    return state;
};


const apiDocsReducer = (state = {
      apidocs : []
}, action) => {
    switch (action.type) {
        case "SET_APIDOCS":
            state = {
                ...state,
                apidocs: action.payload
            };
            // console.log(state.apiDocs);
            break;
    }
    return state;
};


const myLogger = (store) => (next) => (action) => {
    console.log("Logged Action: ", action);
    next(action);
};

const store = createStore(
    combineReducers({math: mathReducer, user: userReducer, apidocs: apiDocsReducer}),
    (state = {}) => state,
    applyMiddleware(logger())
);

function hashLinkScroll() {
  const { hash } = window.location;
  console.log(hash);
  if (hash !== '') {
    // Push onto callback queue so it runs after the DOM is updated,
    // this is required when navigating from a different page so that
    // the element is rendered on the page before trying to getElementById.
    setTimeout(() => {
      const id = hash.replace('#', '');
      const element = document.getElementById(id);
      console.log(element);
      if (element) element.scrollIntoView();
    }, 0);
  }
}

ReactDOM.render(
  <Provider store={store}>
       <Router history={browserHistory} routes={routes}  onUpdate={hashLinkScroll} />
   </Provider>,
  document.getElementById('root')
);
