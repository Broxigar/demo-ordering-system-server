var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var multer  = require('multer')
var dbServices = require('../moudle/db-service');
var passport = require('../config/passport');
var elasticlunr = require('elasticlunr');
var lunr = require('lunr-chinese')();
var lunrCnIndexs = require('../Search/lunrCnIndexs.json')
var paypal = require('paypal-rest-sdk');
var dateFormat = require('dateformat');
var printer = require("node-thermal-printer");
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
//var iconv = require('iconv').Iconv;
//var escpos = require('escpos')

//require('../Search/lunr.stemmer.support.js')(elasticlunr);
//require('../Search/lunr.jp.js')(elasticlunr);
// var db = require('../moudle/db');

paypal.configure({
'mode': 'sandbox', //sandbox or live  
'client_id': 'AbgxupZgkNg5AXzZ6eBsPxxp0KmMARfh4ZKOZc33k1sxZeP56eXTptOCItIxN48wIoYl5h3svEvAK2mn', //对应 paypal 商业账号：mypaypal@qq.com
'client_secret': 'EN1jvjpxnGC4GqPx-w7NRACYwpl2nbrzEuj0zA38D6uY8OOA9X2XeeWeu6VERcjz7GduKq31gigQ1sdS'  
});



/* GET home page. */
router.get('/', function(req, res, next) {
res.sendFile('/client/build/index.html');
});
router.get('/log', function(req,res,next){
console.log(__dirname)
// res.sendFile(path.join(__dirname, '../nohup.out'));
fs.createReadStream(path.join(__dirname, '../nohup.out'))
.pipe(res);
});

var servicesMap = {};
function reloadService(){
dbServices.services(function(err, data){
  //console.log(data);
  console.log(err);
for(var i=0; i<data.length; i++){
  // console.log(data[i].name);
  var api = data[i].API;
  var apiName = String(api.split('|')[0].trim());
  servicesMap[apiName] = data[i];
}
console.log("Loading servicesMap...");
// console.log(servicesMap);
});
}

reloadService();



router.get('/reload', function(req,res,next){
  reloadService();
  res.json({"refresh":"success"});
});

var idx = lunr();
idx.ref('id');

idx.field('title');
idx.field('body');




Search = (pram)=>{
//console.log(idx)

  var search = idx.search(pram);
  return search;
}


router.get('/searchIndex/:search', function(req,res,next){

  var s = Search(req.params.search);
  console.log(s);
  //res.json(s);
  res.json(s);
});  




router.get('/allservices', function(req, res, next) {
// dbServices.services(function(err, data){
// console.log(data);
var data = Object.values(servicesMap);
console.log("Print values:");
console.log(data);
objs = [];
for(var i=0; i<data.length; i++){
  // console.log(data[i].API);
  obj = {};
  obj.id = data[i].id;
  objArray = data[i].API.split('|');
  obj.serviceName = objArray[0].trim();
  obj.sql = objArray[1].toUpperCase();
  obj.info = objArray[2];
  obj.params = [];
  obj.returns = [];
  obj.method = "";
  sqlArray = obj.sql.trim().split(/[,\n=\s+]+/);
  // console.log(sqlArray[0].toUpperCase());
  if(sqlArray[0].toUpperCase() === "SELECT") obj.method = "GET";
  if(sqlArray[0].toUpperCase() === "INSERT") obj.method = "POST";
  if(sqlArray[0].toUpperCase() === "DELETE") obj.method = "DELETE";
  if(sqlArray[0].toUpperCase() === "UPDATE") obj.method = "PUT";
  // console.log(sqlArray);
  for(var j=0; j<sqlArray.length; j++){

    if(sqlArray[j].toUpperCase()==='AS'){
      obj.returns.push(sqlArray[j+1]);
    }
    if(sqlArray[j] === "?"){
      obj.params.push(sqlArray[j-1]);
    }
  }
  // console.log(obj);
  objs.push(obj);
}
res.json(objs);
// });
// res.json(servicesMap);
});

router.get('/api/:service', function(req, res, next) {
var serviceName = req.params.service.trim();

if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
var sql = servicesMap[serviceName].API.split('|')[1];
dbServices.query(sql, function(error, dd){
  console.log(error);
  // console.log(dd);
  res.json(dd)
});
}else{
console.log("Query service failed");
}
});
router.get('/api1/:service/:param1', function(req, res, next) {
var serviceName = req.params.service.trim();

if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
var sql = servicesMap[serviceName].API.split('|')[1];
dbServices.query(sql, [req.params.param1],function(error, dd){
  console.log(error);
  // console.log(dd);
  res.json(dd);
});
}else{
console.log("Query service failed");
}
});

router.get('/api2/:service/:param1/:param2', function(req, res, next) {
var serviceName = req.params.service.trim();

if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
var sql = servicesMap[serviceName].API.split('|')[1];
dbServices.query(sql, [req.params.param1, req.params.param2],function(error, dd){
  console.log(error);
  console.log(dd);
  res.json(dd);
});
}else{
console.log("Query service failed");
}
});


router.post('/service', passport.authenticate('jwt'), function(req, res) {
console.log("post service obj:")
console.log(req.body);
dbServices.saveService(req.body, function(err, row){
console.log(err);
res.json(row);
});
});

router.post('/api/:service',function(req, res) {
var serviceName = req.params.service.trim();
if(servicesMap[serviceName]!=null && servicesMap[serviceName]!=undefined){
var sql = servicesMap[serviceName].API.split('|')[1];
console.log(sql)
// delete req.body[serviceName]
console.log(req.body)
dbServices.query(sql, [req.body],function(error, dd){
  console.log(error);
  console.log(dd);
  if(error){
    res.json(error);

}
else {
  if ((req.body.name!=null && req.body.name!=undefined)&& (req.body.content!=null && req.body.content!=undefined)){
    
    var index= {
      "id": dd.insertId,
      "title": req.body.name,
      "body": req.body.content,

    }
    
    idx.add(index);
    

    res.json(dd);
  }
  else res.json({ message: 'Title or Content is empty!' })
  
}
  
});
}else{
console.log("Query service failed");
}
});

router.post('/test', passport.authenticate('jwtUser'),function(req, res) {
res.json({'authenticated':'yes'});
})




var createFolder = function(folder){
try{
  fs.accessSync(folder);
}catch(e){
  fs.mkdirSync(folder);
}
};

// var uploadFolder = './upload/img/';

// createFolder(uploadFolder);

// //通过 filename 属性定制

// //var filenames = [];
// var storage = multer.diskStorage({
// destination: function (req, file, cb) {
//     cb(null, uploadFolder);    // 保存的路径，备注：需要自己创建
// },
// filename: function (req, file, cb) {
//     // 将保存文件名设置为 时间戳+原名，比如 1478521468943-logo
//     // console.log("######### orginal name: "+file.originalname);
//     var name = Date.now()+'-'+file.originalname;
    
//     //filenames.push(name);
//     cb(null, name);

// }
// });

// // 通过 storage 选项来对 上传行为 进行定制化
//  var uploadStorage = multer({ storage: storage })

//新建 预定单
router.post('/Bookingneworder',function(req, res) {
  console.log("Bookingneworder")
  var a =0;   
  //var day = dateFormat(new Date(), "isoDateTime");
  var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
  var sql = 'INSERT INTO ordersBooking (customerName, customerPhoneNO, customerNumber, bookingComment, bookingDateTime,  totalPrice, createTime, comment, tableID, bookingStatus ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

  console.log(req.body.creatTime)
  dbServices.query(sql, [req.body.customerName, req.body.customerPhoneNo, req.body.customerNumber, req.body.bookingComment, req.body.bookingDateTime, req.body.totalPrice, day,  req.body.comment, req.body.tableID, 1 ], function(error,dd){
          
    if(error){
      return res.json(error);
    }
    else{
      var id  = dd.insertId;
      var v = req.body.items;
      var objs = [];
      for (let i =0;i<v.length;i++){
        var obj = [v[i].num, id, v[i].DishID, v[i].type, v[i].subtype, 0];
        objs.push(obj);
      }
      console.log("Bookingneworder " + objs);
      
      var sq = 'INSERT INTO ItemsBooking (num, orderBookingID, DishID, type, subtype, status) VALUES ?'
        dbServices.query(sq, [objs], function(error){ 
          if(error){
            return res.json(error);
          }
          else {
            var sqll = 'update tables set ? where id = ?';
            dbServices.query(sqll, [{status: 3, currentOrderBookingID: id},req.body.tableID], function(error){  
              if(error){
                return res.json(error);
              }
              else {
                res.json({success: true, msg: 'Successfully created the new order'});
              }
            });
          }
          });
    }
  });
  })

// router.post('/neworder',function(req, res) {
//   var a =0;
//   //var sql = 'insert into orders set ?'
//   var day = dateFormat(new Date(), "isoDateTime");
//   var sql = 'INSERT INTO orders (creatTime, totalPrice, tableID, status, type, comment) VALUES (?, ?, ?, ?, ?, ?)';
//   //console.log(req.body.id)
//   console.log(req.body.creatTime)
//   dbServices.query(sql, [day, req.body.totalPrice, req.body.tableID, 1, 1, req.body.comment], function(error,dd){
      
//     if(error){
//       return res.json(error);
//     }
//     else{

//       var id  = dd.insertId;
  
//       //res.json({success: true, msg: id});
  
//       var v = req.body.items;
//       var objs = [];
//       for (let i =0;i<v.length;i++){
//         var obj = [v[i].num, id, v[i].DishID, v[i].type];
//         // var obj ={};
//         // obj.orderID = id;
//         // obj.DishID = v[i].DishID;
//         // obj.DishCount = v[i].num;
//         objs.push(obj);
//       }
//       console.log("297 " + objs);
  
//       var sq = 'INSERT INTO Items (DishCount, orderID, DishID, type) VALUES ?'
  
//         dbServices.query(sq, [objs], function(error){
          
//           if(error){
        
//             return res.json(error);
        
//           }
//           else {
//             var sqll = 'update tables set ? where id = ?';
//             dbServices.query(sqll, [{status: 2, currentOrderID: id},req.body.tableID], function(error){
                
//               if(error){
//                 return res.json(error);
//               }
//               else {
//                 res.json({success: true, msg: 'Successfully created the new order'});
            
//               }
//             });
//           }
      
//          });
  
  

//     }
//   });


router.post('/neworder',
passport.authenticate(['jwtUser', 'jwtAdmin']),function(req, res) {
  var a =0;
  //var sql = 'insert into orders set ?'
  //var day = dateFormat(new Date(), "isoDateTime");
  var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
  var sql = 'INSERT INTO orders (creatTime, totalPrice, tableID, status, type, comment) VALUES (?, ?, ?, ?, ?, ?)';
  //console.log(req.body.id)
  console.log(req.body.creatTime)
  dbServices.query(sql, [day, req.body.totalPrice, req.body.tableID, 1, 1, req.body.comment], function(error,dd){
        
  if(error){
    return res.json(error);
  }
  else{

    var id  = dd.insertId;
    
    //res.json({success: true, msg: id});
    
    var v = req.body.items;
    var objs = [];
    for (let i =0;i<v.length;i++){
      var obj = [v[i].num, id, v[i].DishID, v[i].type, v[i].comment];
      // var obj ={};
      // obj.orderID = id;
      // obj.DishID = v[i].DishID;
      // obj.DishCount = v[i].num;
      objs.push(obj);
    }
    console.log("297 " + objs);
    
    var sq = 'INSERT INTO Items (DishCount, orderID, DishID, type, comment) VALUES ?'
    
      dbServices.query(sq, [objs], function(error){
            
        if(error){
          
          return res.json(error);
          
        }
        else {
          var sqll = 'update tables set ? where id = ?';
          dbServices.query(sqll, [{status: 2, currentOrderID: id},req.body.tableID], function(error){
                  
            if(error){
              return res.json(error);
            }
            else {
              res.json({success: true, msg: 'Successfully created the new order'});
              
            }
          });
        }
        
        });

  
  
  router.post('/Bookingneworder',function(req, res) {
    console.log("Bookingneworder")
    var a =0;
    //var day = dateFormat(new Date(), "isoDateTime");
    var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
    var sql = 'INSERT INTO ordersBooking (customerName, customerPhoneNO, customerNumber, bookingComment, bookingDateTime,  totalPrice, createTime, comment, tableID, bookingStatus ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

    console.log(req.body.creatTime)
    dbServices.query(sql, [req.body.customerName, req.body.customerPhoneNo, req.body.customerNumber, req.body.bookingComment, req.body.bookingDateTime, req.body.totalPrice, day,  req.body.comment, req.body.tableID, 1 ], function(error,dd){
            
      if(error){
        return res.json(error);
      }
      else{
  
        var id  = dd.insertId;
        var v = req.body.items;
        var objs = [];
        for (let i =0;i<v.length;i++){
          var obj = [v[i].num, id, v[i].DishID, v[i].type, v[i].subtype];
          objs.push(obj);
        }
        console.log("Bookingneworder " + objs);
        
        var sq = 'INSERT INTO ItemsBooking (num, orderBookingID, DishID, type, subtype) VALUES ?'
        
          dbServices.query(sq, [objs], function(error){
                
            if(error){
              
              return res.json(error);
              
            }
            else {
              var sqll = 'update tables set ? where id = ?';
              dbServices.query(sqll, [{status: 3, currentOrderBookingID: id},req.body.tableID], function(error){
                      
                if(error){
                  return res.json(error);
                }
                else {
                  res.json({success: true, msg: 'Successfully created the new order'});
                  
                }
              });
            }
            
           });
        
        
  
      }
    });
    
    })

}
});

})

router.post('/Deliveryneworder',function(req, res) {

//var sql = 'insert into orders set ?'
var sql = 'INSERT INTO orders (creatTime, totalPrice, status, type) VALUES (?, ?, ?, ?)';
//console.log(req.body.id)
//var day = dateFormat(new Date(), "isoDateTime");
var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
dbServices.query(sql, [day, req.body.totalPrice, 1, 2], function(error,dd){
        
  if(error){
    return res.json(error);
  }
  else{

    var id  = dd.insertId;
    
    //res.json({success: true, msg: id});
    
    var v = req.body.items;
    var objs = [];
    for (let i =0;i<v.length;i++){
      var obj = [v[i].num, id, v[i].DishID];
      objs.push(obj);
    }
    console.log(objs);
    
    var sq = 'INSERT INTO Items (DishCount, orderID, DishID) VALUES ?'
    
      dbServices.query(sq, [objs], function(error){
            
        if(error){
          
          return res.json(error);
          
        }
        else {
          var sqll = 'INSERT INTO deliveryDetails (orderID, deliveryFee, name, address, phone) VALUES ?';
          dbServices.query(sqll, id, req.body.deliveryFee, req.body.name, req.body.address, req.body.phone, function(error){
                  
            if(error){
              return res.json(error);
            }
            else {
              if (req.body.redeemedPoints===1){
                var sqllll = 'select * from members where id = ?'
                dbServices.query(sqllll, req.body.memberID, function(error, dd){
            
                  if(error){
                    
                    return res.json(error);
                    
                  }
                  else {
                
                    var discountAmount = Math.floor(dd[0].points / 100);
                    var amount = discountAmount * 100;
                    var discount = discountAmount * 5;
                    var remainder = dd[0].points - amount + req.body.totalPrice;
                    var deduct = -amount;
                    //var day = dateFormat(new Date(), "isoDateTime");
                    var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
                    console.log(deduct)
                    console.log(day)
                    console.log(req.body.orderID)
                    console.log(remainder)
                    var sqlllll = 'INSERT INTO pointsHistory (memberID, amount, date, orderID) VALUES (?, ?, ?, ?)';
                    var data = [req.body.memberID, deduct, day, id];
                    dbServices.query(sqsqlllll, [req.body.memberID, deduct, day, id], function(error){
                      if (error){
                        console.log(error)
                        return res.json(error);
                      }
                      else {
                        var sqll = 'update members set ? where id = ?'
                          dbServices.query(sqll, [{points: remainder}, req.body.memberID], function(error){

                            if(error){
                              return res.json(error);
                            }
                            else{ 
                              //var data = {"discountAmount": discountAmount, "discount": discount}         
                              res.json({success: true, msg: 'Successfully created the new delivery order'});
                            }
                          });
                      }
                    })
                  }
                  })
              }
              else {
                res.json({success: true, msg: 'Successfully created the new delivery order'})
              }
              ;
              
            }
          });
        }
        
        });
    
    

  }
});

})

router.post('/addDish',function(req, res){
var sql = 'INSERT INTO dishmod (orderID, DishID, num, createTime, name, price, type, comment) VALUES ?';
//console.log(req.body.id)
//console.log(req.body.createTime)
var objs = [];
var v = req.body.items
var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
//console.log(v.length);
if (req.body.items.length >0) {
  for (var i =0; i<v.length;i++){
  
    var obj = [v[i].orderID, v[i].DishID, v[i].DishCount, day, v[i].name, v[i].price, v[i].type, v[i].comment];
    
    //console.log(obj)
    objs.push(obj);
  }
    //console.log(objs)
    dbServices.query(sql, [objs], function(error){
      
      if(error){
        return res.json(error);
      }
      else {
        if (req.body.comment){
          var sqll = 'update orders set ? where id = ?'
            dbServices.query(sqll, [{comment: req.body.comment},req.body.orderID], function(error){

              if(error){
                return res.json(error);
              }
              else{  
                
                    res.json({success: true, msg: 'Successfully modified dishes'});
                      
                
              }
            });

        }
        else {
          res.json({success: true, msg: 'Successfully modified dishes'});
        }
      }
    
    })

}
else if (req.body.comment){
  var sqll = 'update orders set ? where id = ?'
            dbServices.query(sqll, [{comment: req.body.comment},req.body.orderID], function(error){

              if(error){
                return res.json(error);
              }
              else{          
                res.json({success: true, msg: 'Successfully modified comment'});
              }
            });
}

else {
  res.json({success: true, msg: 'No change to commit'});
}






  
})


router.post('/modDish',function(req, res) {

var sql = 'INSERT INTO dishmod (orderID, DishID, num, createTime, name, price, type) VALUES ?';
//console.log(req.body.id)
//console.log(req.body.createTime)
var objs = [];
var v = req.body.items
var currentPrice 
//console.log(v.length);
for (var i =0; i<v.length;i++){
  
  var obj = [v[i].orderID, v[i].DishID, v[i].DishCount, v[i].createTime, v[i].name, v[i].price, v[i].type];
  
  //console.log(obj)
  objs.push(obj);
}
  //console.log(objs)
  dbServices.query(sql, [objs], function(error){
    
    if(error){
      return res.json(error);
    }
    else {
          res.json({success: true, msg: 'Successfully modified dishes'});
        
    }
  
  })

})
//删除新增菜品时调用 并把原始新增菜品 从数据库删除
router.post('/deleteModDish',function(req, res) {

  var sql = 'INSERT INTO dishmod (orderID, DishID, num, createTime, name, price, type) VALUES ?';

  //console.log(req.body.id)
  //console.log(req.body.createTime)
  var objs = [];
  var v = req.body.items
  //console.log(v.length);
  for (var i =0; i<v.length;i++){
    
    var obj = [v[i].orderID, v[i].DishID, v[i].num, v[i].createTime, v[i].name, v[i].price, v[i].type];
    
    //console.log(obj)
    objs.push(obj);
  }
    //console.log(objs)
    dbServices.query(sql, [objs], function(error){
      
      if(error){
        return res.json(error);
      }
      else {
            res.json({success: true, msg: 'Successfully modified dishes'});
      }
    })
  })

router.post('/checkout',function(req, res) {

var sql = 'update orders set ? where id = ?'
console.log("aaa"+req.body.paymentMethod)
dbServices.query(sql, [{status: 4, finalPrice: req.body.finalPrice, paymentMethod: req.body.paymentMethod},req.body.orderID], function(error){

if(error){
  res.json(error);
}
else{
  var sqll = 'update tables set ? where id = ?';
  dbServices.query(sqll, [{status: 1, currentOrderID: null},req.body.tableID], function(error){
          
    if(error){
      return res.json(error);
    }
    else if (req.body.phone !== null && req.body.phone !== undefined && req.body.phone !== ""){

        var sq = 'select * from members where phone = ?';
        dbServices.query(sq, req.body.phone, function(error, dd){
        
          if(error){
            return res.json(error);
          }
          else if (dd.length>0){
              
              var finalpoints = parseInt(dd[0].points) + parseInt(req.body.finalPrice);
              console.log("dd[0].points" + dd[0].points)
              console.log("finalPrice" + req.body.finalPrice)
              console.log(finalpoints)
              var addPoints = finalpoints;
              console.log("addPoints" + addPoints)
              //var day = dateFormat(new Date(), "isoDateTime");
              var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 

              var sq = 'INSERT INTO pointsHistory (memberID, amount, date, orderID) VALUES (?, ?, ?, ?)';
              //var data = [dd[0].id, dd[0].points, day, req.body.orderID];
              console.log("Memberid" + dd[0].id)
              dbServices.query(sq, [dd[0].id, addPoints, day, req.body.orderID], function(error){
                if (error){
                  console.log(error)
                  return res.json(error);
                }
                else {
                  var sqlll = 'update members set ? where id = ?'
                    dbServices.query(sqlll, [{points: finalpoints}, dd[0].id], function(error){

                      if(error){
                        return res.json(error);
                      }
                      else{          
                        res.json({success: true, msg: 'Successfully checked out'});
                      }
                    });
                }
              })
            
          }
          else {
            return res.json({success: false, msg: 'Phone does not exist.'})
          }
        
        }) 
        
      }
      else {
        return res.json({success: true, msg: 'Successfully checked out without adding points.'})
      }
  });
}
});

})

//上传 唯一 指定更改的菜品信息
router.post('/updateDishInfo',function(req, res) {
  var sql = 'update dish set ? where dishId = ?';
  dbServices.query(sql, [{name: req.body.dishNameValue, price: req.body.dishPriceValue, type: req.body.dishTypeValue, subtype: req.body.dishSubtypeValue },req.body.dishIdValue], function(error){  
    if(error){
      return res.json(error);
    }
    else {
      res.json({success: true, msg: 'Successfully updated dish Info'});
    }
  });
  })

//上传 新增 菜品信息
router.post('/addNewDishInfo',function(req, res) {
  var sql = 'INSERT INTO dish (name, price, type, subtype) VALUES (?, ?, ?, ?)';
  console.log(req.body.NewdishNameValue)
  console.log(req.body.NewdishPriceValue)
  console.log(req.body.NewdishTypeValue)
  console.log(req.body.NewdishSubtypeValue)
  dbServices.query(sql, [ req.body.NewdishNameValue, req.body.NewdishPriceValue, req.body.NewdishTypeValue, req.body.NewdishSubtypeValue ], function(error){  
    if(error){
      return res.json(error);
    }
    else {
      res.json({success: true, msg: 'Successfully add the new dish Info'});
    }
  });
  })

router.post('/cash', (req, res) => {

  var sql = 'INSERT INTO orders (creatTime, status, totalPrice, type, comment, paymentStatus) VALUES (?, ?, ?, ?, ?, ?)';
  //var day = dateFormat(new Date(), "isoDateTime");
  var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
  
  dbServices.query(sql, [day, 2, req.body.totalPrice, 2, req.body.comment, 1], function(error, dd){
                  
    if(error){
      return res.json(error);
      console.log(error);
    }
    else {
      var orderID  = dd.insertId;
      var objs = [];
      var v = JSON.parse(req.body.items)
      
      //console.log(v[0].num)
      for (let i=0;i<v.length;i++){
        
          var obj = [v[i].num, orderID, v[i].DishID];
          objs.push(obj);  
      }
        var sqll = 'INSERT INTO Items (DishCount, orderID, DishID) VALUES ?'
        dbServices.query(sqll, [objs], function(error){
          console.log(error)
          if(error){
            return res.json(error);
          }
          else {
            
            var sql5 = 'INSERT INTO deliveryDetails (orderID, deliveryFee, name, address, phone) VALUES (?, ?, ?, ?, ?)'
            
            dbServices.query(sql5, [orderID, req.body.shipping, req.body.recipent_name, req.body.address, req.body.phone], function(error){
            console.log(error)
              if(error){
                return res.json(error);
              }
              else {
                var sqlll = 'select * from members where phone = ?'
                dbServices.query(sqlll, req.body.phone, function(error, dd){
            
                  if(error){
                    
                    return res.json(error);
                    
                  }
                  else if (dd.length>0){
                   
                    //var day = dateFormat(new Date(), "isoDateTime");
                    var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
                    
                    var sqllll = 'INSERT INTO pointsHistory (memberID, amount, date, orderID) VALUES ?';
                    var data = [];
                    //var deduct = [dd[0].id, deduct, day, orderID];
                    //var add = [dd[0].id, addAmount,day, orderID];
                    var discountAmount = Math.floor(dd[0].points / 100);
                    var amount = discountAmount * 100;
                    var discount = discountAmount * 5;
                    //var remainder = dd[0].points - amount
                    var remainder = dd[0].points - amount + Math.floor(req.body.totalPrice);
                    var addAmount = Math.floor(req.body.totalPrice);
                    var deduct = -amount;
                    if (req.body.DiscountAmount !== null && req.body.DiscountAmount!== undefined && req.body.DiscountAmount>0){
                      
                        remainder = dd[0].points - amount + Math.floor(req.body.totalPrice);
                        var deduc = [dd[0].id, deduct, day, orderID];
                        data.push(deduc);
                      }
                      else {
                        remainder = parseInt(dd[0].points-0) + Math.floor(req.body.totalPrice);
                        //console.log("remainder: "+ remainder)
                      }
                      //console.log(flag)
                      console.log(remainder)
                      if (addAmount >0){
                        var add = [dd[0].id, addAmount,day, orderID];
                        data.push(add);
                      }
                      
                      dbServices.query(sqllll, [data], function(error){
                        if (error){
                          console.log(error)
                          return res.json(error);
                        }
                        else {
                          var sqlllll = 'update members set ? where phone = ?'
                            dbServices.query(sqlllll, [{points: remainder}, req.body.phone], function(error){
  
                              if(error){
                                return res.json(error);
                              }
                              else{ 
                                //var data = {"discountAmount": discountAmount, "discount": discount}         
                                res.json({success: true, msg: 'Successfully created the new delivery order'});
                                
                              }
                            });
                        }
                      })    
                  }
                  else {
                    res.json({success: true, msg: 'Successfully created the new delivery order'});
                  }
                })
          }
        
        })
      }
    })
      }
    })
})

router.post('/paypal', (req, res) => {
  console.log (req.body.items[0])
  if (req.body.totalPrice == 0){
    console.log("asfasdfasfasfsadf")
    var des = req.body.comment;
    var sql = 'INSERT INTO orders (creatTime, status, totalPrice, type, comment, paymentStatus) VALUES (?, ?, ?, ?, ?, ?)';
    //var day = dateFormat(new Date(), "isoDateTime");
    var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
      dbServices.query(sql, [day, 2, req.body.totalPrice, 2, des, 2], function(error, dd){
              
              if(error){
                return res.json(error);
                console.log(error);
              }
              else {
                var orderID  = dd.insertId;
                var objs = [];
                var v = JSON.parse(req.body.items) 
                for (let i=0;i<v.length;i++){
                  
                    var obj = [v[i].num, orderID, v[i].DishID];
                    objs.push(obj);
                  
                }
                  var sq = 'INSERT INTO Items (DishCount, orderID, DishID) VALUES ?'
                  dbServices.query(sq, [objs], function(error){
                    console.log(error)
                    if(error){
                      return res.json(error);
                    }
                    else {
                     
                      var sqll = 'INSERT INTO deliveryDetails (orderID, deliveryFee, name, address, phone) VALUES (?, ?, ?, ?, ?)'

                      dbServices.query(sqll, [orderID, req.body.shipping, req.body.recipent_name, req.body.address, req.body.phone], function(error){
                      console.log(error)
                        if(error){
                          return res.json(error);
                        }
                        else {
                          var sqllll = 'select * from members where phone = ?'
                          dbServices.query(sqllll, req.body.phone, function(error, dd){
                      
                            if(error){
                              
                              return res.json(error);
                              
                            }
                            else {
                              var discountAmount = Math.floor(dd[0].points / 100);
                              var amount = discountAmount * 100;
                              var discount = discountAmount * 5;
                              var remainder = dd[0].points - amount;
                              //var addAmount = Math.floor(a.amount.total);
                              var deduct = -amount;
                              //var day = dateFormat(new Date(), "isoDateTime");
                              var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
                              
                              var sqlllll = 'INSERT INTO pointsHistory (memberID, amount, date, orderID) VALUES (?, ?, ?, ?)';
                              //var data = [];
                              //var deduct = [dd[0].id, deduct, day, orderID];
                              //var add = [dd[0].id, addAmount,day, orderID];
                              //data.push(deduct);
                              //data.push(add);
                              console.log(dd[0].id)
                              console.log(deduct)
                              console.log(day)
                              console.log(orderID)
                              
                              dbServices.query(sqlllll, [dd[0].id, deduct, day, orderID], function(error){
                                if (error){
                                  console.log(error)
                                  return res.json(error);
                                }
                                else {
                                  var sqll = 'update members set ? where phone = ?'
                                    dbServices.query(sqll, [{points: remainder}, req.body.phone], function(error){
      
                                      if(error){
                                        return res.json(error);
                                      }
                                      else{ 
                                        //var data = {"discountAmount": discountAmount, "discount": discount}         
                                        res.redirect("http://localhost:3000/PaySucceed");
                                        
                                      }
                                    });
                                  }
                                })
                              }
            
                          })
                        }
                      })
                    }
                  })        
              }
              
            })
          }
  else if (req.body.totalPrice > 0){
    var v = JSON.parse(req.body.items);
    console.log(v.length)
    var items = []
    for (let i =0;i<v.length;i++){
    var item = {
      "name": v[i].name,
      "sku": v[i].DishID,
      "price": v[i].price,
      "currency": "AUD",
      "description": "",
      "quantity": v[i].num
      
      };
    items.push(item);
    }


    if (req.body.DiscountAmount !== null && req.body.DiscountAmount!== undefined && req.body.DiscountAmount>0){

    var d = -req.body.DiscountAmount;
    var Ditem = {
      "name": "Discount",
      "sku": "0",
      "price": d,
      "currency": "AUD",
      "description": "",
      "quantity": 1
      };
    // console.log(Ditem)
    items.push(Ditem);

    }
    console.log(req.body.totalPrice)
      const create_payment_json = {
          "intent": "sale",
          "payer": {
              "payment_method": "paypal"
          },
          "redirect_urls": {
              "return_url": "http://api.shuweiyuan.com.au/success",
              "cancel_url": "http://api.shuweiyuan.com.au/cancel"
          },
          "transactions": [{
              "item_list": {
                  "items": items,
                  "shipping_address": {
                    "recipient_name": req.body.recipent_name,
                    "line1": req.body.address,
                    "city": "Canberra",
                    "state": "ACT",
                    "country_code": "AU",
                    "postal_code": '2617',
                },
                "shipping_phone_number": req.body.phone
              },
              "amount": {
                  "currency": "AUD",
                  "total": req.body.totalPrice,
                  "details": {
                      "subtotal": req.body.DishPrice,
                    //"tax": "0.07",
                    "shipping": req.body.shipping
                  }
              },
              "description": req.body.comment
          }]
      };


      paypal.payment.create(create_payment_json, function (error, payment) {
        console.log("xc")
          if (error) {
            console.log(error.response.details);
              throw error;
          } else {
              for(let i = 0; i < payment.links.length;i++) {
                  if(payment.links[i].rel === 'approval_url') {
                    console.log(payment.links[i].href)
                      //res.redirect(payment.links[i].href);
                      res.json({success: true, msg: payment.links[i].href}); 
                  }
              }
          }
      });
  }
        
  else {
    res.json({success: false, msg: "Payment failed, please retry."}); 
  }
  
  
});

router.get('/cancel', (req, res)=>{
  res.redirect("http://order.shuweiyuan.com.au/PayCancelled");

})

router.get('/success', (req, res) => {
  console.log(req)
  const payerId = req.query.PayerID;
  const paymentId = req.query.paymentId;
  var execute_payment_json = {
    "payer_id" : req.query.PayerID
  };

  // var execute_payment_json = {
  //     "payer_id": payerId,
  //     "transactions": [{
  //         "amount": {
  //             "currency": "AUD",
  //             "total": "14.00"
  //         }
  //     }]
  // };

  paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
    console.log("asdfasf")
      if (error) {
          console.log(error.response);
          //throw error;
          return res.send('Payment failed, please go back and retry.')
      } else {
        
        if (payment.state == 'approved'){
          
          var des = null;
        var sql = 'INSERT INTO orders (creatTime, status, totalPrice, type, comment, paymentStatus) VALUES (?, ?, ?, ?, ?, ?)';
        
        if (payment.transactions[0].description) {
          des = payment.transactions[0].description;
        }
        else {
          des = "";
        }
        console.log(des)
        console.log(payment.create_time);
        console.log(payment.transactions[0].amount.total);
        var obs=[payment.create_time, 2,payment.transactions[0].amount.total, 2, des];

        console.log(obs)
       // var day = dateFormat(new Date(), "isoDateTime");
       var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
          dbServices.query(sql, [day, 2, payment.transactions[0].amount.total, 2, des, 2], function(error, dd){
                  
                  if(error){
                    return res.json(error);
                    console.log(error);
                  }
                  else {
                    var orderID  = dd.insertId;
                    var objs = [];
                    for (let i=0;i<payment.transactions[0].item_list.items.length;i++){
                      if (payment.transactions[0].item_list.items[i].name != "Discount"){
                        var obj = [payment.transactions[0].item_list.items[i].quantity, orderID, payment.transactions[0].item_list.items[i].sku];
                        objs.push(obj);
                      }
                      
                    }
                      var sq = 'INSERT INTO Items (DishCount, orderID, DishID) VALUES ?'
                      dbServices.query(sq, [objs], function(error){
                        console.log(error)
                        if(error){
                          return res.json(error);
                        }
                        else {
                          var a = payment.transactions[0];
                          var sqll = 'INSERT INTO deliveryDetails (orderID, deliveryFee, name, address, phone) VALUES (?, ?, ?, ?, ?)'
                          var deliObjs = [orderID, a.amount.details.shipping, a.item_list.shipping_address.recipient_name, a.item_list.shipping_address.line1, a.item_list.shipping_phone_number];

                          dbServices.query(sqll, [orderID, a.amount.details.shipping, a.item_list.shipping_address.recipient_name, a.item_list.shipping_address.line1, a.item_list.shipping_phone_number], function(error){
                          console.log(error)
                            if(error){
                              return res.json(error);
                            }
                            else {
                              var sql6 = 'INSERT INTO paypalPayment (orderID, createTime, paymentID) VALUES (?, ?, ?)';
                              console.log("aaaaaaaaaaaaaaa")
                              dbServices.query(sql6, [orderID, day, paymentId], function(error){
                                console.log(error)
                                  if(error){
                                    return res.json(error);
                                  }
                                  else {
                                    var flag = 0;
                                    console.log(flag);
                                    for (let i =0;i<a.item_list.items.length;i++){
                                      if (a.item_list.items[i].name === "Discount"){
                                        flag =1;
                                        //return;
                                      }
                                    }
                                    console.log(flag)
                                    
                                      console.log("cccccccccccccccc")
                                      console.log(a.item_list.shipping_phone_number)
                                      var sqllll = 'select * from members where phone = ?'
                                      dbServices.query(sqllll, a.item_list.shipping_phone_number, function(error, dd){
                                  
                                        if(error){
                                          
                                          return res.json(error);
                                          
                                        }
                                        else if (dd.length>0){
                                          console.log(dd[0])
                                          console.log(dd[0].points)
                                          console.log(discountAmount)
                                          console.log(amount)
                                          console.log(dd[0].id)
                                          var discountAmount = Math.floor(dd[0].points / 100);
                                          var amount = discountAmount * 100;
                                          var discount = discountAmount * 5;
                                          var remainder = null;
                                          var addAmount = Math.floor(a.amount.total);
                                          var deduct = -amount;
                                          //var day = dateFormat(new Date(), "isoDateTime");
                                          var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
                                          var data = [];
                                          if (flag === 1){
                                            remainder = dd[0].points - amount + Math.floor(a.amount.total);
                                            var deduc = [dd[0].id, deduct, day, orderID];
                                            data.push(deduc);
                                          }
                                          else {
                                            remainder = parseInt(dd[0].points-0) + Math.floor(a.amount.total);
                                            console.log("remainder: "+ remainder)
                                          }
                                          console.log(flag)
                                          console.log(remainder)
                                          if (addAmount >0){
                                            var add = [dd[0].id, addAmount,day, orderID];
                                            data.push(add);
                                          }
                                         
                                          
                                          var sqlllll = 'INSERT INTO pointsHistory (memberID, amount, date, orderID) VALUES ?';

                                          dbServices.query(sqlllll, [data], function(error){
                                            if (error){
                                              console.log(error)
                                              return res.json(error);
                                            }
                                            else {
                                              
                                              var sqll = 'update members set ? where phone = ?'
                                                dbServices.query(sqll, [{points: remainder}, a.item_list.shipping_phone_number], function(error){
                  
                                                  if(error){
                                                    return res.json(error);
                                                  }
                                                  else{ 
                                                    //var data = {"discountAmount": discountAmount, "discount": discount}         
                                                    //res.json({success: true, msg: 'Successfully created the new delivery order'});
                                                    res.redirect("http://order.shuweiyuan.com.au/PaySucceed");
                                                  }
                                                });
                                            }
                                          })
                                        }
                                        else {
                                          res.redirect("http://order.shuweiyuan.com.au/PaySucceed");
                                        }
                                        })
                                    //}////////////////////////////////
                                    // else {
                                    //   //res.json({success: true, msg: 'Successfully created the new delivery order'})
                                    //   res.redirect("http://localhost:3000/PaySucceed");
                                    // }
                                    //res.json({success: true, msg: 'Successfully placed the order.'});
                                  }
                                })
                              
                            }
                          })
                        }
                      })
                  }
                  
                })
        } 
        else {
          return res.json({success: false, msg: 'The payment failed.'})
        }

        
      }
  });
});

router.get('/cancel', (req, res) => res.send('Cancelled'));

router.post('/pointsInfo',function(req, res) {

var sql = 'select * from members where phone = ?';

dbServices.query(sql, req.body.phone, function(error, dd){
  
  if(error){
    return res.json(error);
  }
  else if (dd.length>0)
  {
    
      var points= {"points":dd[0].points, "redeemablePoints": Math.floor(dd[0].points / 100), "discount": Math.floor(dd[0].points / 100)*5};
      return res.json({success: true, msg: points})
    
    
    
  }
  else {
    return res.json({success: false, msg: 'Phone does not exist.'})
  }

})

})

router.post('/redeem',function(req, res) {

var sql = 'select * from members where phone = ?';
console.log(req.body.phone)
  dbServices.query(sql, req.body.phone, function(error, dd){
    console.log(dd)
    if(error){
      return res.json(error);
    }
    else if (dd.length>0){
      if (dd[0].points < 100){
        return res.json({success: false, msg: 'Not enough points'})
      }
      else {
        console.log(dd[0].id)
        var discountAmount = Math.floor(dd[0].points / 100);
        var amount = discountAmount * 100;
        var discount = discountAmount * 5;
        var remainder = dd[0].points - amount;
        var deduct = -amount;
        //var day = dateFormat(new Date(), "isoDateTime");
        var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
        console.log(deduct)
        console.log(day)
        console.log(req.body.orderID)
        console.log(remainder)
        var sq = 'INSERT INTO pointsHistory (memberID, amount, date, orderID) VALUES (?, ?, ?, ?)';
        var data = [dd[0].id, deduct, day, req.body.orderID];
        dbServices.query(sq, [dd[0].id, deduct, day, req.body.orderID], function(error){
          if (error){
            console.log(error)
            return res.json(error);
          }
          else {
            var sqll = 'update members set ? where id = ?'
              dbServices.query(sqll, [{points: remainder}, dd[0].id], function(error){

                if(error){
                  return res.json(error);
                }
                else{ 
                  var data = {"discountAmount": discountAmount, "discount": discount}         
                  res.json({success: true, discount: data});
                }
              });
          }
        })
      }
    }
    else {
      return res.json({success: false, msg: 'Phone does not exist.'})
    }
  
  })

})


router.post('/PrintOrigDishes',function(req, res) {

  var sql = 'UPDATE Items SET printed = printed + 1 WHERE id = ?';
  
  var v = req.body.OrderedOrigDish;
  //var v = JSON.parse(req.body.OrderedOrigDish);
  //console.log("AAA ")
  //console.log(req.body.OrderedOrigDish[0].id)
    //console.log(v.length)
          for (let i =0;i<v.length;i++){
            var obj = parseInt(JSON.stringify(v[i].id));
            //console.log(obj)
           if (i === v.length-1){
            dbServices.query(sql, obj , function(error, result){
        
              if(error){
                return res.json({success: false, msg: error});
              }
              
              else {
                //console.log(result.affectedRows + " record(s) updated");
                return res.json({success: true, msg: 'Modified dishes have been printed out.'})
              }
            
            })
           }
           else {
            dbServices.query(sql, obj , function(error, result){
        
              if(error){
                return res.json({success: false, msg: error});
                //break;
              }
              
            
            })
           }
            
          }
          
  // var objs = [];
  //       for (let i =0;i<v.length;i++){
  //         var obj = [v[i].id];
  //         objs.push(obj);
  //       }
  //   dbServices.query(sql, [objs], function(error){
      
  //     if(error){
  //       return res.json({success: false, msg: error});
  //     }
      
  //     else {
  //       return res.json({success: true, msg: 'Original dishes have been printed out.'})
  //     }
    
  //   })
  })

  router.post('/PrintModDishes',function(req, res) {

    var sql = 'UPDATE dishmod SET printed = printed + 1 WHERE id = ?';
    //console.log(req.body.phone)
    var v = req.body.OrderedModDish;
    //console.log(v)
          for (let i =0;i<v.length;i++){
            var obj = parseInt(JSON.stringify(v[i].id));
            //console.log(obj)
           if (i === v.length-1){
            dbServices.query(sql, obj , function(error, result){
        
              if(error){
                return res.json({success: false, msg: error});
              }
              
              else {
                //console.log(result.affectedRows + " record(s) updated");
                return res.json({success: true, msg: 'Modified dishes have been printed out.'})
              }
            
            })
           }
           else {
            dbServices.query(sql, obj , function(error, result){
        
              if(error){
              
                return res.json({success: false, msg: error});
              
              }
              
            
            })
           }
            
          }
          
      
    })
 
router.get('/Stats/:service', function(req, res, next){
  var serviceName = req.params.service.trim();
  console.log(serviceName)
  //var day = dateFormat();
  //moment().local();
  var currentDateEnd = moment().local().utcOffset("+10:00").format("YYYY-MM-DDT23:59:59"); 
  var currentDateStart = moment().local().utcOffset("+10:00").format("YYYY-MM-DDT00:00:00");
  //console.log(currentDate)
  //new Date(),"yyyy-mm-dd'T'HH:MM:ss o"
  //var currentD = moment().local().utcOffset("+10:00").format("YYYY-MM-DD%"); 
  var d = moment(currentDateStart)
  var week = d.subtract(1, 'week').utcOffset("+10:00").format("YYYY-MM-DDT23:59:59")
  console.log(week)
  var month = d.subtract(1, 'month').utcOffset("+10:00").format("YYYY-MM-DDT23:59:59")
  console.log(month)
  //var substractD = d.subtract(1, 'week').utcOffset("+10:00").format("YYYY-MM-DD%")
  //var substractD =substractDate+"%"
  // var startOrder = null
  // var endOrder = null
  // var sqlStartOrder = 'select * from orders where creatTime like ? order by id limit 1';
  // var sqlEndOrder = 'select * from orders where creatTime like ? order by id desc limit 1';  
  // var sqlLatestOrder = 'select * from orders order by id desc limit 1'; 
   var Stat = 'select sum(T.DishCount) total,  T.DishID, dish.name from (SELECT sum( DishCount ) DishCount,\
   i.DishID FROM Items i, orders where i.deleted =0 and i.orderID = orders.id and \
    orders.creatTime <= STR_TO_DATE(?, "%Y-%c-%eT%H:%i:%s" ) and creatTime >=STR_TO_DATE(?, "%Y-%c-%eT%H:%i:%s" )\
     GROUP BY i.DishID union all \
   SELECT sum( num ) DishCount , d.DishID FROM dishmod d, orders where d.deleted =0 and d.orderID = orders.id \
   and creatTime <= STR_TO_DATE(?, "%Y-%c-%eT%H:%i:%s" ) and orders.creatTime >=STR_TO_DATE(?, "%Y-%c-%eT%H:%i:%s" )\
   GROUP BY d.DishID ) T, dish where T.DishID = dish.dishid group by T.DishID order by total desc'
  if (serviceName === 'week') {
    dbServices.query(Stat, [currentDateEnd, week, currentDateEnd, week] , function(error, result){
        
      if(error){
        return res.json({success: false, msg: error});
      }
      
      else {
        return res.json({success: true, stat: result, date: week + "--"+ currentDateEnd})
      }
    
    })
  }
  if (serviceName === 'day') {
    dbServices.query(Stat, [currentDateEnd, currentDateStart, currentDateEnd, currentDateStart] , function(error, result){
        
      if(error){
        return res.json({success: false, msg: error});
      }
      
      else {
        return res.json({success: true, stat: result, date: currentDateStart + "--"+ currentDateEnd})
      }
    
    })

  }
  if (serviceName === 'month') {
    dbServices.query(Stat, [currentDateEnd, month, currentDateEnd, month] , function(error, result){
        
      if(error){
        return res.json({success: false, msg: error});
      }
      
      else {
        return res.json({success: true, stat: result, date: month + "--"+ currentDateEnd})
      }
    
    })

  }
        
})     

router.get('/getCurrentTime', function(req, res, next){
//var day = dateFormat();
//moment().local();
var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
//new Date(),"yyyy-mm-dd'T'HH:MM:ss o"
return res.json(day);
})

router.get('/getCurrentDate', function(req, res, next){
  var day = moment().local().utcOffset("+10:00").format("dddd, YYYY-MM-DD"); 
  return res.json(day);
  })

router.get('/todayStat',function(req, res) {
  var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DDTHH:mm:ss"); 
  var date = day.slice(0, day.indexOf("T"))+"%"
  //console.log(date)
  var sqlOrderCount = 'SELECT count(id) as count FROM orders where creatTime like ? and status =4';
  var sqlCashSum = 'SELECT sum(finalPrice) as cash FROM orders where creatTime like ? and paymentMethod = 1';
  var sqlCardSum = 'SELECT sum(finalPrice) as card FROM orders where creatTime like ? and paymentMethod = 2';
  var sqlTransferSum = 'SELECT sum(finalPrice) as transfer FROM orders where creatTime like ? and paymentMethod = 3';

  dbServices.query(sqlOrderCount, date, function(error, count){
    
    if(error){
      return res.json(error);
    }
    else 
    {
      dbServices.query(sqlCashSum, date, function(error, cash){
    
        if(error){
          return res.json(error);
        }
        else 
        {
          dbServices.query(sqlCardSum, date, function(error, card){
    
            if(error){
              return res.json(error);
            }
            else 
            {
              dbServices.query(sqlTransferSum, date, function(error, transfer){
    
                if(error){
                  return res.json(error);
                }
                else 
                {
                  return res.json({success: true, count: count[0].count, cash: cash[0].cash, card: card[0].card, transfer: transfer[0].transfer })
                      
                }
              
              
              })
                  
            }
          
          
          })
              
        }
      
      
      })
    }
  
  
  })
  
  
  
  })

  
  router.get('/getAllTodayOrders',function(req, res) {
    //var day = dateFormat(new Date(),"isoDateTime");
    //var date = dateFormat(new Date(),"yyyy-mm-dd");
    var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DD"); 
    var date = day+"%"
    //console.log(date)
    //var date = '2019-01-19%'
    var sql = `Select Items.orderID, Items.deleted, Items.DishCount, dish.name, dish.ename, dish.price, orders.finalPrice, orders.type as OrderType, orders.creatTime 
    from Items, dish, orders 
    where Items.DishID = dish.dishId and Items.orderID = orders.id and orders.id in (SELECT DISTINCT id FROM orders where orders.status = 4 and creatTime like ?)
    union all
    select dishmod.orderID, dishmod.deleted, dishmod.num, dish.name, dish.ename, dish.price, orders.finalPrice, orders.type as OrderType, orders.creatTime
    from dishmod, orders, dish
    where dishmod.DishID = dish.dishId and dishmod.orderID = orders.id and orders.id in (SELECT DISTINCT id FROM orders where orders.status = 4 and creatTime like ?)
    order by orderID`;
  
    
    dbServices.query(sql, [date, date], function(error, dd){
      
      if(error){
        return res.json(error);
      }
      else
      {
        //return res.json({success: true, TodayOrders: dd})
        //console.log(dd)
         var sqll = "SELECT DISTINCT id FROM orders where orders.status = 4 and creatTime like ? order by id"
         dbServices.query(sqll, date, function(error, distinctID){
      
           if(error){
            return res.json(error);
          }
          else {
            
            return res.json({success: true, TodayOrders: dd, DistinctID: distinctID})  
                        
       }
      
    })


        
      }
    
    })
  })

  router.get('/getAllTodayOrdersDESC',function(req, res) {
    //var day = dateFormat(new Date(),"isoDateTime");
    //var date = dateFormat(new Date(),"yyyy-mm-dd");
    var day = moment().local().utcOffset("+10:00").format("YYYY-MM-DD"); 
    var date = day+"%"
    //console.log(date)
    //var date = '2019-01-19%'
    var sql = `Select Items.orderID, Items.deleted, Items.DishCount, dish.name, dish.ename, dish.price, orders.finalPrice, orders.type as OrderType, orders.creatTime 
    from Items, dish, orders 
    where Items.DishID = dish.dishId and Items.orderID = orders.id and orders.id in (SELECT DISTINCT id FROM orders where orders.status = 4 and creatTime like ?)
    union all
    select dishmod.orderID, dishmod.deleted, dishmod.num, dish.name, dish.ename, dish.price, orders.finalPrice, orders.type as OrderType, orders.creatTime
    from dishmod, orders, dish
    where dishmod.DishID = dish.dishId and dishmod.orderID = orders.id and orders.id in (SELECT DISTINCT id FROM orders where orders.status = 4 and creatTime like ?)
    order by orderID DESC`;
  
    
    dbServices.query(sql, [date, date], function(error, dd){
      
      if(error){
        return res.json(error);
      }
      else
      {
        //return res.json({success: true, TodayOrders: dd})
        //console.log(dd)
         var sqll = "SELECT DISTINCT id FROM orders where orders.status = 4 and creatTime like ? order by id DESC"
         dbServices.query(sqll, date, function(error, distinctID){
      
           if(error){
            return res.json(error);
          }
          else {
            
            return res.json({success: true, TodayOrders: dd, DistinctID: distinctID})  
                        
       }
      
    })


        
      }
    
    })
  })

  router.post('/getCipher',function(req, res) {
    
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
          return res.json(err);
      }

      bcrypt.hash(req.body.pw, salt, null, function (err, hash) {
          if (err) {
            return res.json(err);
          }
          return res.json({success: true, msg: hash})
        })

  })
})


// router.get('/print', function(req, res, next){
//  const networkDevice  = new escpos.Network('10.0.0.131', 8899);
// // const options = { encoding: "GB18030" /* default */ }
//  const printer = new escpos.Printer(networkDevice);
// // escpos.USB.findPrinter()
// // consolo.log("a"+a);
//   //const device  = new escpos.USB(0x6868, 0x0200);
//   //const options = { encoding: "EUC-JP" /* default */ }
//   //const printer = new escpos.Printer(device);
// //   let s = new iconv('EUC-CN', 'GB18030')
// networkDevice.open(function(){
//     printer
//     .font('a')
//     .align('ct')
//     .style('normal')
//     .size(1, 1)
//     .text("中文")
//     .text("はじめまして。私はアンナです")
//     .text('สวัสดี')
//     .encode('ISO-8859-1') // set encode globally
//     .text("华文字幕")
//     .text("fuck")
// //.text('동해물과 백두산이 마르고 닳도록')
//     //.barcode('1234567', 'EAN8')
//     .cut()
//     .close();

//     });
// })


router.post('/printReceipt', function(req, res){
 console.log(req.body.tableID)
  printer.init({
  type: 'epson',
  interface: 'tcp://10.0.0.131:9100',
  //characterSet: 'CHINA',
  width: 80
  });


  var day = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
  printer.alignCenter();
  printer.setTextDoubleHeight();                      // Set text to double height
  printer.setTextDoubleWidth(); 
  printer.println("INVOICE");
  printer.setTextNormal();
  printer.println(" ");
  printer.setTextDoubleHeight();
  printer.println("SICHUAN CHINESE RESTAURANT");
  printer.setTextNormal();
  printer.println(" ");
  printer.println("------------------------------------------------")
  printer.println(" ");
  printer.println("Table "+ req.body.tableID);
  printer.println(day)
  printer.println(" ");
  printer.println("------------------------------------------------");
  
  // printCate = (items, category) =>{

  //     printer.alignLeft();
  //     if (category === "orderNormal"){
  //       printer.println("Normal Dishes");
  //     }
  //     else if (category === "orderSDHP"){
  //       printer.println("Spicy Dried Hotpot");
  //     }
  //     else {
  //       printer.println("Fish");
  //     }
  //     printer.println(" ");
  //     var v = items
  //     for (let i =0; i< v.length; i++){
  //       printer.alignLeft();
  //       var a;
  //       var dc;
  //       if (v[i].DishCount != undefined &&v[i].DishCount != null){
  //         dc = v[i].DishCount
  //         a = v[i].price * v[i].DishCount
  //       }
  //       else {
  //         dc = v[i].num
  //         a = v[i].price * v[i].num
  //       }
        
  //       printer.println(dc+ "  x    "+ v[i].name)
  //       printer.alignRight();
  //       printer.println("$"+a)
  //     }
  //     printer.println(" ");
    
  // }
  if (req.body.orderNormal.length>0){
    printer.alignCenter();
    printer.bold(true); 
    printer.println("Normal Dishes 炒菜", true);
    printer.bold(false); 
    printer.alignLeft();
    printer.println(" ");
    var v = req.body.orderNormal
    for (let i =0; i< v.length; i++){
      printer.alignLeft();
      var a;
      var dc;
      if (v[i].DishCount != undefined &&v[i].DishCount != null){
        dc = v[i].DishCount
        a = v[i].price * v[i].DishCount
      }
      else {
        dc = v[i].num
        a = v[i].price * v[i].num
      }
      
      printer.println(dc+ "  x    "+ v[i].name, true)
      printer.alignRight();
      printer.println("$"+a)
      
    }
    printer.println("________________________________________________");
    printer.println(" ");
  }
  if (req.body.orderSDHP.length>0){
    printer.alignCenter();
    printer.bold(true); 
    printer.println("Spicy Dried Hotpot 麻辣香锅", true);
    printer.bold(false); 
    printer.alignLeft();
    printer.println(" ");
    var v = req.body.orderSDHP
    for (let i =0; i< v.length; i++){
      printer.alignLeft();
      var a;
      var dc;
      if (v[i].DishCount != undefined &&v[i].DishCount != null){
        dc = v[i].DishCount
        a = v[i].price * v[i].DishCount
      }
      else {
        dc = v[i].num
        a = v[i].price * v[i].num
      }
      
      printer.println(dc+ "  x    "+ v[i].name, true)
      printer.alignRight();
      printer.println("$"+a)
    }
    printer.println("________________________________________________");
    printer.println(" ");
  }
  if (req.body.orderFish.length>0){
    printer.alignCenter();
    printer.bold(true); 
    printer.println("Fish");
    printer.bold(false); 
    printer.alignLeft();
    var v = req.body.orderFish
    for (let i =0; i< v.length; i++){
      printer.alignLeft();
      var a;
      var dc;
      if (v[i].DishCount != undefined &&v[i].DishCount != null){
        dc = v[i].DishCount
        a = v[i].price * v[i].DishCount
      }
      else {
        dc = v[i].num
        a = v[i].price * v[i].num
      }
      
      printer.println(dc+ "  x    "+ v[i].name, true)
      printer.alignRight();
      printer.println("$"+a)
    }
    printer.println("________________________________________________");
    printer.println(" ");
  }
  if (req.body.DeliveryFee != undefined && req.body.DeliveryFee >0){
    printer.alignLeft();
    printer.println("Delivery fee   "+ req.body.DeliveryFee)
    printer.println(" ");
  }
  printer.println("------------------------------------------------");
  printer.println(" ");
  printer.alignRight();
  if (req.body.DeliveryDishPrice != undefined && req.body.DeliveryDishPrice >0){
    printer.println("Subtotal " + "$"+req.body.DeliveryDishPrice);
    printer.bold(true);
    printer.println("Total Price " + "$"+req.body.DeliveryTotalPrice);
    printer.bold(false);
  }
  else {
    if (req.body.DineInDiscount != undefined && req.body.DineInDiscount>0){
      printer.println("Subtotal " + "$"+req.body.DineInTotalPrice);
      printer.println("Discount " + "$"+req.body.DineInDiscount);
      printer.bold(true);
      printer.println("Total Price " + "$"+req.body.DineInDiscountedPrice);
      printer.bold(false);
    }
    else {
      printer.println("Subtotal " + "$"+req.body.DineInTotalPrice);
      printer.bold(true);
      printer.println("Total Price " + "$"+req.body.DineInDiscountedPrice);
      printer.bold(false);
    }
  }

  printer.cut();
  //let s = new iconv('utf8', 'cp1251')
  printer.isPrinterConnected( function(isConnected){
  //console.log(isConnected);
  if(isConnected){
      //printer.cut();
      printer.execute(function(err){
          if (err) {
              console.log("Print failed! Reason : "+err);
              res.json(err);
          } else {
            console.log("Test print done");
            //res.send("Print done.")
            res.json({print: 'SUCCESS'});
          }
      });
  }else{
    console.log("printer is not connected! Please check it!");
  }
});

});
  

router.post('/KprinterN', function(req, res){
  if (req.body.orderNormal.length>0){
   printer.init({
   type: 'epson',
   interface: 'tcp://10.0.0.131:8899',
   //characterSet: 'CHINA',
   width: 80
   });
 
 
   var day = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
   var time = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
   printer.alignCenter();
   printer.setTextDoubleHeight();                      // Set text to double height
   printer.setTextDoubleWidth(); 
   printer.println("炒菜 对单");
   printer.setTextNormal();
   printer.println(" ");
   printer.alignRight();
   printer.println(day);
   printer.setTextNormal();
   printer.println(" ");
   printer.println("================================================")
   printer.alignCenter();
   printer.println("Table "+ req.body.tableID);
   printer.println("================================================");


     printer.alignLeft();
     printer.println(" ");
     var v = req.body.orderNormal
     printer.setTextDoubleHeight();
     printer.setTextDoubleWidth();
     printer.bold(true);
     for (let i =0; i< v.length; i++){
       printer.alignLeft();
       var a;
       var dc;
       if (v[i].DishCount != undefined &&v[i].DishCount != null){
         dc = v[i].DishCount
         //a = v[i].price * v[i].DishCount
       }
       else {
         dc = v[i].num
         //a = v[i].price * v[i].num
       }
       
       printer.println(dc+ "  "+ v[i].name)
       
     }
     printer.setTextNormal();
     printer.bold(false);
     printer.println("================================================");
     printer.println(" ");
  
   printer.cut();
   //let s = new iconv('utf8', 'cp1251')
   printer.isPrinterConnected( function(isConnected){
   //console.log(isConnected);
   if(isConnected){
       //printer.cut();
       printer.execute(function(err){
          if (err) {
              console.log("Print failed! Reason : "+err);
              res.json(err);
          } else {
            console.log("Test print done");
            //res.send("Print done.")
            res.json({print: 'SUCCESS'});
          }
       });
   }else{
     console.log("printer is not connected! Please check it!");
   }
  });
}
 });

router.post('/KprinterS', function(req, res){
  if (req.body.orderSDHP.length>0){
   printer.init({
   type: 'epson',
   interface: 'tcp://10.0.0.131:8899',
   //characterSet: 'CHINA',
   width: 80
   });
 
 
   var day = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
   var time = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
   printer.alignCenter();
   printer.setTextDoubleHeight();                      // Set text to double height
   printer.setTextDoubleWidth(); 
   printer.println("麻辣香锅 对单");
   printer.setTextNormal();
   printer.println(" ");
   printer.alignRight();
   printer.println(day);
   printer.setTextNormal();
   printer.println(" ");
   printer.println("================================================")
   printer.alignCenter();
   printer.println("Table "+ req.body.tableID);
   printer.println("================================================");


     printer.alignLeft();
     printer.println(" ");
     var v = req.body.orderSDHP
     printer.setTextDoubleHeight();
     printer.setTextDoubleWidth();
     printer.bold(true);
     for (let i =0; i< v.length; i++){
       printer.alignLeft();
       var a;
       var dc;
       if (v[i].DishCount != undefined &&v[i].DishCount != null){
         dc = v[i].DishCount
         //a = v[i].price * v[i].DishCount
       }
       else {
         dc = v[i].num
         //a = v[i].price * v[i].num
       }
       
       printer.println(dc+ "  "+ v[i].name)
       
     }
     printer.setTextNormal();
     printer.bold(false);
     printer.println("================================================");
     printer.println(" ");
  
   printer.cut();
   //let s = new iconv('utf8', 'cp1251')
   printer.isPrinterConnected( function(isConnected){
   //console.log(isConnected);
   if(isConnected){
       //printer.cut();
       printer.execute(function(err){
          if (err) {
              console.log("Print failed! Reason : "+err);
              res.json(err);
          } else {
            console.log("Test print done");
            //res.send("Print done.")
            res.json({print: 'SUCCESS'});
          }
       });
   }else{
     console.log("printer is not connected! Please check it!");
   }
 });
}
 });

 router.post('/KprinterF', function(req, res){
  if (req.body.orderFish.length>0){
   printer.init({
   type: 'epson',
   interface: 'tcp://10.0.0.131:8899',
   //characterSet: 'CHINA',
   width: 80
   });
 
 
   var day = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
   var time = dateFormat("dddd, mmmm dS, yyyy, h:MM:ss TT");
   printer.alignCenter();
   printer.setTextDoubleHeight();                      // Set text to double height
   printer.setTextDoubleWidth(); 
   printer.println("特色烤鱼 对单");
   printer.setTextNormal();
   printer.println(" ");
   printer.alignRight();
   printer.println(day);
   printer.setTextNormal();
   printer.println(" ");
   printer.println("================================================")
   printer.alignCenter();
   printer.println("Table "+ req.body.tableID);
   printer.println("================================================");


     printer.alignLeft();
     printer.println(" ");
     var v = req.body.orderFish
     printer.setTextDoubleHeight();
     printer.setTextDoubleWidth();
     printer.bold(true);
     for (let i =0; i< v.length; i++){
       printer.alignLeft();
       var a;
       var dc;
       if (v[i].DishCount != undefined &&v[i].DishCount != null){
         dc = v[i].DishCount
         //a = v[i].price * v[i].DishCount
       }
       else {
         dc = v[i].num
         //a = v[i].price * v[i].num
       }
       
       printer.println(dc+ "  "+ v[i].name)
       
     }
     printer.setTextNormal();
     printer.bold(false);
     printer.println("================================================");
     printer.println(" ");
  
   printer.cut();
   //let s = new iconv('utf8', 'cp1251')
   printer.isPrinterConnected( function(isConnected){
   //console.log(isConnected);
   if(isConnected){
       //printer.cut();
       printer.execute(function(err){
           if (err) {
               console.log("Print failed! Reason : "+err);
               res.json(err);
           } else {
             console.log("Test print done");
             //res.send("Print done.")
             res.json({print: 'SUCCESS'});
           }
       });
   }else{
     console.log("printer is not connected! Please check it!");
   }
 });
}
 });

// //多图上传
// router.post('/newPost', uploadStorage.array('file',6), function(req, res, next){
// var data = req.body; 
// data.urlImages = JSON.stringify(filenames);
// delete data['Content-Type'];
// filenames = [];
// // console.log(data);
// var sql = 'insert into POST set ?';
// dbServices.query(sql, [data], function(err, row){
// // console.log(err);
// // console.log(row);

// if(!err){
//   res.json({result: 'SUCCESS'});
// }else{
//   res.json({result: 'ERROR'});
// }
// });

// });
// // 单图上传
// router.post('/uploadImg', uploadStorage.single('file'), function(req, res, next){
// // var data = req.body;

// const filename = req.file.filename;
// //var a = upIMG().file;
// //const filename = a[0]

// console.log(filename)
// res.json({filename: filename})
// });

router.get('/img/:name', function(req, res, next){
var form = fs.readFileSync('menuPIC/img/'+req.params.name);
res.send(form);
});

router.post('/IMG/:name', function(req, res, next){
var form = fs.readFileSync('upload/img/'+req.params.name);
res.send(form);
});

router.get('/time', function(req, res, next){
var day = dateFormat(new Date(), "isoDateTime");
res.send(day)
});

module.exports = router;
