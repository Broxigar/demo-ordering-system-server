import React, {ReactDOM} from 'react';
import { Link } from 'react-router';

const ApiDocDisplay = (props) => {
  const dataArray = [];
  const menuArray = [];
  if(props.docs.apidocs.length > 0){
    props.docs.apidocs.map((apidoc, index) =>{
      dataArray.push(<ApiDocItem className="list-item" key={index} info={apidoc} />);
      // console.log(apidoc);
      menuArray.push(<ApiDocMenu key={index} menu={apidoc.serviceName} />);
    });
    return (
      <div className="row">
          <div className="col-xs-3">
            <div className="list-group">
              <a href="" className="list-group-item list-group-item-info"> APIs </a>
              {menuArray}
            </div>
          </div>
          <div className="col-xs-9">
              {dataArray}
          </div>
      </div>
    );
  }else{
    return (
      <div className="row">
      </div>
    );
  }
};

const ApiDocMenu = (props) => {
  return (
    <Link className="list-group-item" to={'/apiInfo#'+props.menu} > {props.menu}</Link>
  );
}

const ApiDocItem = (props) => {
  return (
    <div  id={props.info.serviceName}>
    <table className="table table-striped" >
      <tbody>
        <tr className="info">
          <td colSpan="2">Service Name: {props.info.serviceName}</td>
        </tr>
        <tr>
          <td className="col-xs-2 centre "><code>Info</code></td>
          <td>{props.info.info}</td>
        </tr>
        <tr>
          <td className="col-xs-2 centre "><code>URI</code></td>
          <td> localhost:3000/{props.info.serviceName}</td>
        </tr>
        <tr>
          <td className="col-xs-2 centre "><code>METHOD</code></td>
          <td>{props.info.method}</td>
        </tr>
        <tr>
          <td className="col-xs-2 centre "><code>PARAM</code></td>
          <td>Params &#123;
                 {props.info.params.map((r,i)=>{
                  return (<div> &nbsp;&nbsp;&nbsp;&nbsp; <code>{r}</code> <br/> </div>);
                })} &#125;</td>
        </tr>
        <tr>
          <td className="col-xs-2 centre "><code>RETURN</code></td>
          <td>{props.info.serviceName} &#123;
                 {props.info.returns.map((r,i)=>{
                  return (<div> &nbsp;&nbsp;&nbsp;&nbsp; <code>{r}</code> <br/> </div>);
                })} &#125;
          </td>
        </tr>
        <tr>
          <td className="col-xs-2 centre "><code>SQL</code></td>
          <td>{props.info.sql}</td>
        </tr>
        </tbody>
    </table>
    </div>
  );
};



export default ApiDocDisplay;
